/******************************************************************************
 * @file
 * @brief USART/UART Asynchronous mode Application Note software example
 * @author Energy Micro AS
 * @version 1.01
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2012 Energy Micro AS, http://www.energymicro.com</b>
 ******************************************************************************
 *
 * This source code is the property of Energy Micro AS. The source and compiled
 * code may only be used on Energy Micro "EFM32" microcontrollers.
 *
 * This copyright notice may not be removed from the source code nor changed.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: Energy Micro AS has no
 * obligation to support this Software. Energy Micro AS is providing the
 * Software "AS IS", with no express or implied warranties of any kind,
 * including, but not limited to, any implied warranties of merchantability
 * or fitness for any particular purpose or warranties against infringement
 * of any proprietary rights of a third party.
 *
 * Energy Micro AS will not be liable for any consequential, incidental, or
 * special damages, or any other relief, or for any claim by any third party,
 * arising from your use of this Software.
 *
 *****************************************************************************/
 
/******************************************************************************
 * Modified by Erik Lothe 
 *****************************************************************************/

 
 
#include "uart.h"
#include "em_device.h"
#include "em_chip.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_usart.h"
#include "bsp.h"

#include "uart.h"

/* Function prototypes */
void uart_setup(void);
void cmuSetup(void);
void uartPutData(uint8_t * dataPtr, uint32_t dataLen);
uint32_t uartGetData(uint8_t * dataPtr, uint32_t dataLen);
void    uartPutChar(uint8_t charPtr);
uint8_t uartGetChar(void);


/* Declare some strings */
const char     welcomeString[]  = "\n\rBOOTING\n\r";
const char     overflowString[] = "overflow\n\r";
const uint32_t welLen           = sizeof(welcomeString) - 1;
const uint32_t ofsLen           = sizeof(overflowString) - 1;

volatile struct circularBuffer
{
  uint8_t  data[BUFFERSIZE];  /* data buffer */
  uint32_t rdI;               /* read index */
  uint32_t wrI;               /* write index */
  uint32_t pendingBytes;      /* count of how many bytes are not yet handled */
  bool     overflow;          /* buffer overflow indicator */
} rxBuf, txBuf = { {0}, 0, 0, 0, false };


/* Setup UART1 in async mode for RS232*/
static USART_TypeDef           * uart   = USART1;
static USART_InitAsync_TypeDef uartInit = USART_INITASYNC_DEFAULT;

bool new_data = false;

/******************************************************************************
 * @brief  Initialization
 *****************************************************************************/
void uart_init(void) {

    /* Initialize clocks and oscillators */
    cmuSetup();

    /* Initialize UART peripheral */
    uart_setup();

    /* Initialize Development Kit in EBI mode */
    BSP_Init(BSP_INIT_DEFAULT);

    /* Enable RS-232 transceiver on Development Kit */
    //  BSP_PeripheralAccess(BSP_RS232_UART, true);

    /* When DVK is configured, and no more DVK access is needed, the interface can safely be disabled to save current */
    BSP_Disable();

    /* Write welcome message to UART */
    uart_put_data((uint8_t*) welcomeString, welLen);
}

/******************************************************************************
* @brief  uartSetup function
*
******************************************************************************/
void uart_setup(void) {

    /* Enable clock for GPIO module (required for pin configuration) */
    CMU_ClockEnable(cmuClock_GPIO, true);
    /* Configure GPIO pins */
    GPIO_PinModeSet(gpioPortD, 0, gpioModePushPull, 1);       // TX (white cable)
    GPIO_PinModeSet(gpioPortD, 1, gpioModeInput, 1);          // RX (green cable)


    /* Prepare struct for initializing UART in asynchronous mode*/
    uartInit.enable       = usartDisable;   /* Don't enable UART upon intialization */
    uartInit.refFreq      = 0;              /* Provide information on reference frequency. When set to 0, the reference frequency is */
    uartInit.baudrate     = 115200;         /* Baud rate */
    uartInit.oversampling = usartOVS16;     /* Oversampling. Range is 4x, 6x, 8x or 16x */
    uartInit.databits     = usartDatabits8; /* Number of data bits. Range is 4 to 10 */
    uartInit.parity       = usartNoParity;  /* Parity mode */
    uartInit.stopbits     = usartStopbits1; /* Number of stop bits. Range is 0 to 2 */
    uartInit.mvdis        = false;          /* Disable majority voting */
    uartInit.prsRxEnable  = false;          /* Enable USART Rx via Peripheral Reflex System */
    uartInit.prsRxCh      = usartPrsRxCh0;  /* Select PRS channel if enabled */

    /* Initialize USART with uartInit struct */
    USART_InitAsync(uart, &uartInit);

    /* Prepare UART Rx and Tx interrupts */
    USART_IntClear(uart, _USART_IF_MASK);
    USART_IntEnable(uart, USART_IF_RXDATAV);
    NVIC_ClearPendingIRQ(USART1_RX_IRQn);
    NVIC_ClearPendingIRQ(USART1_TX_IRQn);
    NVIC_EnableIRQ(USART1_RX_IRQn);
    NVIC_EnableIRQ(USART1_TX_IRQn);

    /* Enable I/O pins at UART1 location #1 */
    uart->ROUTE = USART_ROUTE_RXPEN | USART_ROUTE_TXPEN | USART_ROUTE_LOCATION_LOC1;
    //  uart->ROUTE = 0b0100000011;

    /* Enable UART */
    USART_Enable(uart, usartEnable);
}



/******************************************************************************
 * @brief  uartGetChar function
 *
 *  Note that if there are no pending characters in the receive buffer, this
 *  function will hang until a character is received.
 *
 *****************************************************************************/
uint8_t uartGetChar( )
{
  uint8_t ch;

  /* Check if there is a byte that is ready to be fetched. If no byte is ready, wait for incoming data */
  if (rxBuf.pendingBytes < 1)
  {
    while (rxBuf.pendingBytes < 1) ;
  }

  /* Copy data from buffer */
  ch        = rxBuf.data[rxBuf.rdI];
  rxBuf.rdI = (rxBuf.rdI + 1) % BUFFERSIZE;

  /* Decrement pending byte counter */
  rxBuf.pendingBytes--;

  return ch;
}

/******************************************************************************
 * @brief  uartPutChar function
 *
 *****************************************************************************/
void uartPutChar(uint8_t ch) {
    /* Check if Tx queue has room for new data */
    if ((txBuf.pendingBytes + 1) > BUFFERSIZE) {
        /* Wait until there is room in queue */
        while ((txBuf.pendingBytes + 1) > BUFFERSIZE);
    }

    /* Copy ch into txBuffer */
    txBuf.data[txBuf.wrI] = ch;
    txBuf.wrI             = (txBuf.wrI + 1) % BUFFERSIZE;

    /* Increment pending byte counter */
    txBuf.pendingBytes++;

    /* Enable interrupt on USART TX Buffer*/
    USART_IntEnable(uart, USART_IF_TXBL);
}



/******************************************************************************
 * @brief  uartPutData function
 *
 *****************************************************************************/
bool uart_put_data(uint8_t * dataPtr, uint32_t dataLen) {
    uint32_t i = 0;

    /* Check if buffer is large enough for data */
    if (dataLen > BUFFERSIZE) {
        /* Buffer can never fit the requested amount of data */
        return false;
    }

    /* Check if buffer has room for new data */
    if ((txBuf.pendingBytes + dataLen) > BUFFERSIZE) {
        /* Wait until room */
        while ((txBuf.pendingBytes + dataLen) > BUFFERSIZE) ;
    }

    /* Fill dataPtr[0:dataLen-1] into txBuffer */
    while (i < dataLen) {
        txBuf.data[txBuf.wrI] = *(dataPtr + i);
        txBuf.wrI             = (txBuf.wrI + 1) % BUFFERSIZE;
        i++;
    }

    /* Increment pending byte counter */
    txBuf.pendingBytes += dataLen;

    /* Enable interrupt on USART TX Buffer*/
    USART_IntEnable(uart, USART_IF_TXBL);
    
    /* Wait for data to be transmitted */
    while (rxBuf.pendingBytes);
    return true;
}

void uart_print(char* str) {
    uint32_t len = length((uint8_t*) str);
    if (!len) return;
    if (!uart_put_data((uint8_t*)str, len)) {
        uart_println("UART ERROR: tried to print too large string");
    }
}

void uart_println(char* str) {
    uart_print(str);
    uart_newline();
}

void uart_print_binary(uint8_t binary) {
    char str[4];
    binary_to_string(str, binary, 4);
    uart_put_data((uint8_t*)str, 4);
}

void uart_print_number(int32_t number) {
    if (number > 99) {
        uint8_t str[5];
        long_int_to_string(str, number);
        uart_put_data(str, 5);
    } else {
        uint8_t str[3];
        int_to_string(str, number);
        uart_put_data(str, 3);
    }
}

/******************************************************************************
 * @brief  uartGetData function
 *
 *****************************************************************************/
uint32_t uartGetData(uint8_t * dataPtr, uint32_t dataLen) {
    uint32_t i = 0;

    /* Wait until the requested number of bytes are available */
    if (rxBuf.pendingBytes < dataLen) {
        while (rxBuf.pendingBytes < dataLen);
    }

    if (dataLen == 0) {
        dataLen = rxBuf.pendingBytes;
    }

    /* Copy data from Rx buffer to dataPtr */
    while (i < dataLen) {
        *(dataPtr + i) = rxBuf.data[rxBuf.rdI];
        rxBuf.rdI      = (rxBuf.rdI + 1) % BUFFERSIZE;
        i++;
    }

    /* Decrement pending byte counter */
    rxBuf.pendingBytes -= dataLen;

    return i;
}

/**************************************************************************//**
 * @brief Receives a command from the backend server
 * Blocks until a command is received, and echoes back command
 *****************************************************************************/
uint32_t uart_get_command(uint8_t* buffer) {
    while (rxBuf.pendingBytes);
    while (!new_data);
    int i = 0;
    uint8_t c;
    do {
        c = uartGetChar();
        if (c == '\b') {
            if (i) {
                i--;
                uart_put_data((uint8_t*)"\b \b", 3);
            }
        } else {
            buffer[i++] = c; 
            uartPutChar(c);
        }
    } while (c != TERMINATION_CHAR);

    uart_newline();

    new_data = false;
    buffer[i-1] = '\0';
    return i;
}

/***************************************************************************//**
 * @brief Set up Clock Management Unit
 ******************************************************************************/
void cmuSetup(void) {
    /* Start HFXO and wait until it is stable */
    /* CMU_OscillatorEnable( cmuOsc_HFXO, true, true); */

    /* Select HFXO as clock source for HFCLK */
    /* CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFXO ); */

    /* Disable HFRCO */
    /* CMU_OscillatorEnable( cmuOsc_HFRCO, false, false ); */

    /* Enable clock for HF peripherals */
    CMU_ClockEnable(cmuClock_HFPER, true);

    /* Enable clock for USART module */
    CMU_ClockEnable(cmuClock_USART1, true);
}

/**************************************************************************//**
 * @brief UART1 RX IRQ Handler
 *
 * Set up the interrupt prior to use
 *
 * Note that this function handles overflows in a very simple way.
 *
 *****************************************************************************/
void USART1_RX_IRQHandler(void) {

    /* Check for RX data valid interrupt */
    if (uart->STATUS & USART_STATUS_RXDATAV) {
        /* Copy data into RX Buffer */
        uint8_t rxData = USART_Rx(uart);
        rxBuf.data[rxBuf.wrI] = rxData;
        rxBuf.wrI             = (rxBuf.wrI + 1) % BUFFERSIZE;
        rxBuf.pendingBytes++;


        /* Flag Rx overflow */
        if (rxBuf.pendingBytes > BUFFERSIZE) {
            rxBuf.overflow = true;
        }

        /* Clear RXDATAV interrupt */
        USART_IntClear(USART1, USART_IF_RXDATAV);
    }
        /* Checks for overflow */
    if (rxBuf.overflow) {
        rxBuf.overflow = false;
        uart_put_data((uint8_t*) overflowString, ofsLen);
    }

    new_data = true;
}

/**************************************************************************//**
 * @brief UART1 TX IRQ Handler
 *
 * Set up the interrupt prior to use
 *
 *****************************************************************************/
void USART1_TX_IRQHandler(void) {
    /* Clear interrupt flags by reading them. */
    USART_IntGet(USART1);

    /* Check TX buffer level status */
    if (uart->STATUS & USART_STATUS_TXBL) {
        if (txBuf.pendingBytes > 0) {
            /* Transmit pending character */
            USART_Tx(uart, txBuf.data[txBuf.rdI]);
            txBuf.rdI = (txBuf.rdI + 1) % BUFFERSIZE;
            txBuf.pendingBytes--;
        }

        /* Disable Tx interrupt if no more bytes in queue */
        if (txBuf.pendingBytes == 0) {
            USART_IntDisable(uart, USART_IF_TXBL);
        }
    }
}

uint32_t length(uint8_t *command) {
    int i;
    for (i = 0; command[i] != TERMINATION_CHAR && command[i] != '\0' && i < BUFFERSIZE; i++);
    return i;
}

bool equals(char *c1, char *c2) {
    uint32_t i;
    uint32_t len = length((uint8_t*)c1);
    if (len != length((uint8_t*)c2)) return false;

    for (i=0; i < len; i++) {
        if (c1[i] != c2[i]) return false;
    }
    return true;
}

/**************************************************************************//**
 * @brief Converts a number to string
 * Converts a number in range (-99, 99) to string
 *****************************************************************************/
void int_to_string(uint8_t *timestamp, int32_t i) {
	if (i > 99) {
    	timestamp[0] = '+';
	    timestamp[1] = '9';
	    timestamp[2] = '9';
	    return;
	    
	} else if (i < -99) {
    	timestamp[0] = '-';
	    timestamp[1] = '9';
	    timestamp[2] = '9';
	    return;
	}

    timestamp[0] = '+';
    timestamp[1] = '0';
    timestamp[2] = '0';
	
	if (i > 0) {
	    while (i > 0) {
		    if (i >= 10) {
			    i -= 10;
			    timestamp[1]++;
			
		    } else {
			    timestamp[2] = '0' + i;
			    i=0;
		    }
	    }
	    
    } else if (i < 0) {
        timestamp[0] = '-';
        while (i < 0) {
		    if (i <= -10) {
			    i += 10;
			    timestamp[1]++;
			
		    } else {
			    timestamp[2] = '0' - i;
			    i=0;
		    }
	    }
    }
}
/* (-99, 9999) */
void long_int_to_string(uint8_t *timestamp, int32_t i) {
	if (i > 9999) {
    	timestamp[0] = '+';
	    timestamp[1] = '9';
	    timestamp[2] = '9';
	    timestamp[3] = '9';
	    timestamp[4] = '9';
	    return;
	    
	} else if (i < -99) {
    	timestamp[0] = '-';
	    timestamp[1] = '9';
	    timestamp[2] = '9';
	    return;
	}

    timestamp[0] = '+';
    timestamp[1] = '0';
    timestamp[2] = '0';
    timestamp[3] = '0';
    timestamp[4] = '0';
	
	if (i > 0) {
	    while (i > 0) {
	        
	        if (i >= 1000) {
	            i-= 1000;
	            timestamp[1]++;
	        
	        } else if (i >= 100) {
	            i-= 100;
	            timestamp[2]++;
	        
		    } else if (i >= 10) {
			    i -= 10;
			    timestamp[3]++;
			
		    } else {
			    timestamp[4] = '0' + i;
			    i=0;
		    }
	    }
	    
    } else if (i < 0) {
        timestamp[0] = '-';
        while (i < 0) {
		    if (i <= -10) {
			    i += 10;
			    timestamp[1]++;
			
		    } else {
			    timestamp[2] = '0' - i;
			    i=0;
		    }
	    }
    }
}

unsigned int power(unsigned int base, unsigned int exp) {
    unsigned int res = 1;
    unsigned int i;
    for (i=0; i<exp; i++) {
        res *= base;
    }
    return res;
}

unsigned int string_to_int(char* str) {
    uint32_t len = length((uint8_t*)str);
    unsigned int val = 0;
    uint32_t i;
    for (i=0; i<len; i++) {
        val += power(10, len-1-i) * (str[i] - '0');
    }
    return val;
}

void binary_to_string(char* str, unsigned int val, unsigned int len) {
    unsigned int i;
    for (i=0; i<len; i++) {
        str[len-i-1] = val & (1 << i)? '1':'0';
    }
}

uint8_t string_to_binary(char* str, unsigned int len) {
    unsigned int i;
    uint8_t val = 0;
    for (i=0; i<len; i++) {
        uint8_t bit = (str[len-1-i] - '0') & 1;
        val |= (bit << i);
    }
    return val;
}


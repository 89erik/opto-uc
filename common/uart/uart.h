#ifndef UART_H_
#define UART_H_

#include <stdint.h>
#include <stdbool.h>

#define TERMINATION_CHAR    '\r'
#define BUFFERSIZE          256

void     uart_init(void);
bool     uart_put_data(uint8_t * dataPtr, uint32_t dataLen);
void     uart_println(char* str);
void     uart_print(char* str);
void     uart_print_binary(uint8_t binary);
void     uart_print_number(int32_t number);
uint32_t uart_get_command(uint8_t* buffer);

// UART utilities
uint32_t length(uint8_t *command);
bool     equals(char *c1, char *c2);
void     int_to_string(uint8_t *timestamp, int32_t i);
void     binary_to_string(char* str, unsigned int val, unsigned int len);
uint8_t  string_to_binary(char* str, unsigned int len);
unsigned int string_to_int(char* str);

#define uart_newline() {uart_put_data((uint8_t*)"\n\r", 2);}


#endif

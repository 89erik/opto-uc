#!/bin/bash
set -e
if [ -z "$1" ]
then
    echo "Give uC project dir as argument"
    exit
fi
cd $1
make flash


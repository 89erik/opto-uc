/******************************************************************************
 * @file
 * @brief First optocoupler test
 * @author Erik Lothe
 *****************************************************************************/

#include "em_device.h"
#include "em_chip.h"
#include "bsp.h"
#include "bsp_trace.h"
#include "em_gpio.h"
#include "em_cmu.h"
#include "segmentlcd.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "uart.h"

/* DATA TYPES */
typedef struct {
    GPIO_Port_TypeDef   port;
    unsigned int        pin;
} port_pin_t;
typedef uint8_t bus_t;  // data type for bus data
typedef uint8_t uchar;  // character type for UART

/* CONSTANTS */
#define DEFAULT_CIRCUIT_DELAY     10
#define PROBE_COUNT               1
#define BUS_WIDTH                 4
#define RESULT_TABLE_HEIGHT       0xF+1

/* HEAP DATA */
uchar results[RESULT_TABLE_HEIGHT][BUS_WIDTH];
volatile uint32_t msTicks;

bool debug   = false;
bool inverse = true;
bool opto    = true;
bool safety  = true;

uint32_t circuit_delay = DEFAULT_CIRCUIT_DELAY;

/* LOCAL PROTOTYPES */
void delay(uint32_t dlyTicks);

void report_mode(char* mode_label, bool mode) {
    uart_print("    ");
    uart_print(mode_label);
    if (mode) {
        uart_println(" on");
    } else {
        uart_println(" off");
    }
}

void report_all_modes() {
    report_mode("debug  ", debug);
    report_mode("inverse", inverse);
    report_mode("opto   ", opto);
    report_mode("safety ", safety);
}


/**************************************************************************//**
 * @brief SysTick_Handler
 * Interrupt Service Routine for system tick counter
 *****************************************************************************/
void SysTick_Handler(void) {
    msTicks++;       /* increment counter necessary in Delay()*/
}

/**************************************************************************//**
 * @brief Delays number of msTick Systicks
 * @param dlyTicks Number of ticks to delay
 *****************************************************************************/
void delay(uint32_t dlyTicks){
    uint32_t curTicks = msTicks;
    while ((msTicks - curTicks) < dlyTicks);
}

/**************************************************************************//**
 * @brief Blinks an LED
 *****************************************************************************/
void led_tick() {
    GPIO->P[gpioPortE].DOUTTGL = (0b11 << 2);
}

/**************************************************************************//**
 * @brief Initializes the material busses and LEDs
 *****************************************************************************/
void init_pins(void) {
    CMU_ClockEnable(cmuClock_GPIO, true);
    int i;
    for (i=4; i<4+BUS_WIDTH; i++) {    
        GPIO_PinModeSet(gpioPortD, i, gpioModeInput, 0);      // IN BUS
    }
    for (i=0; i<BUS_WIDTH*2; i++) {
        GPIO_PinModeSet(gpioPortC, i, gpioModePushPull, 0);   // OUT BUS
    }

    GPIO_PinModeSet(gpioPortE, 2, gpioModePushPull, 1); // LED0
    GPIO_PinModeSet(gpioPortE, 3, gpioModePushPull, 0); // LED1
}

inline unsigned int optobit(bus_t busval, unsigned int bit_index) {
    if (debug) {
        uart_print("    optobit. busval = ");
        uart_print_binary(busval);
        uart_print(", bit index = ");
        uart_print_number(bit_index);
    }
    if (((busval >> bit_index) & 1) == 1) {
        return 0b10;
    } else {
        return 0b01;
    }
}

inline void set_out_bus_raw(bus_t input) {
    GPIO->P[gpioPortC].DOUT = (input & 0b11111111);
}

bool optosafe(bus_t input) {
    int i;
    for (i=0; i<4; i++) {
        if ( ((input >> i*2) & 0b11) == 0b11) {
            return false;
        }
    }
    return true;
}

void set_out_bus(bus_t input) {
    if (opto) {
        if (debug) {
            uart_print("input: ");
            uart_print_binary(input);
            uart_newline();
        }
        unsigned int p[4];
        int i;
        for (i=0; i<4; i++) {
            p[i] = optobit(input, i);
            if (debug) {
                uart_print(", optobit = ");
                uart_print_binary(p[i]);
                uart_newline();
            }
        }
        input = (p[3] << 6) | (p[2] << 4) | (p[1] << 2) | p[0];
        if (debug) {
            uart_print("        input: ");
            uart_print_binary((input>>4));
            uart_print_binary((input));
            uart_newline();
        }
        
        set_out_bus_raw(0);
    
    } else if (safety && !optosafe(input)) {
        uart_println("Error, input not optosafe! (disable safety mode to override)");
        uart_print_binary(input);
        uart_newline();
        return;
    }
    
    set_out_bus_raw(input);
}

inline unsigned int read_in_bus() {
    return (GPIO->P[gpioPortD].DIN >> 4) & 0b1111;
}

/**************************************************************************//**
 * @brief Probes material with given input
 * @param result Pointer to row in result table for material output
 * @param input  Input for material
 *****************************************************************************/
void probe_material(uchar* result, bus_t input, bool reset) {
    int i,j;

    // Reset row
    for (i=0; i<BUS_WIDTH; i++) {
        result[i] = 0;
    }

    // Probe all outputs several times
    for (j=0; j<PROBE_COUNT; j++) {
        set_out_bus(input);
        delay(circuit_delay);
        unsigned int probed = read_in_bus();
        if (reset) {
            set_out_bus(0);
            delay(circuit_delay);
        }

        for (i=0; i<BUS_WIDTH; i++) {
            result[BUS_WIDTH-1-i] = result[BUS_WIDTH-1-i] + ((probed >> i) & 0b1);
        }
    }
    
    // Calculate averages for all outputs
    for (i=0; i<BUS_WIDTH; i++) {
        if (debug && result[i] != 0 && result[i] != PROBE_COUNT) {
            uart_print("Result on input ");
            uart_print_binary(input);
            uart_print(" on bit ");
            uart_print_number(i);
            uart_print(" has conflicting result: ");
            uart_print_number(result[i]);
            uart_print("/");
            uart_print_number(PROBE_COUNT);
            uart_println("");
        }
        
        result[i] = result[i] > (PROBE_COUNT / 2)? '1':'0';
        if (inverse) {
            result[i] = '0' + '1' - result[i];
        }
    }
    led_tick();
}

/**************************************************************************//**
 * @brief Probes material for all rows of result table
 *****************************************************************************/
void sweep() {
    bus_t i;
    for (i = 0; i<RESULT_TABLE_HEIGHT; i++) {
        probe_material(results[i], i, false);
    }
    set_out_bus(0);
}

/**************************************************************************//**
 * @brief Applies a pulse to material. Handles 1 to 500 Hz inclusive.
 *****************************************************************************/
void apply_pulse(unsigned int frequency, unsigned int duration) {
    uint32_t half_wave_duration = (1000 / frequency) >> 1;
    uint32_t wave_start, pulse_start;
    
    pulse_start = msTicks;
    while (msTicks - pulse_start < duration) {
        wave_start = msTicks;
        set_out_bus_raw(0b1111);
        while (msTicks - wave_start < half_wave_duration);

        wave_start = msTicks;
        set_out_bus_raw(0b0000);
        while (msTicks - wave_start < half_wave_duration);
    }
}

/**************************************************************************//**
 * @brief Transfers result row to UART
 *****************************************************************************/
void report_result(unsigned int input, uchar* result) {
    uart_print_binary(input);
    uart_print("    ");
    uart_put_data(result, BUS_WIDTH);
    uart_newline();
}

/**************************************************************************//**
 * @brief Transfers result table to UART
 *****************************************************************************/
void report_results() {
    int i;
    for (i=0; i<RESULT_TABLE_HEIGHT; i++) {
        report_result(i, results[i]);
    }
}

/**************************************************************************//**
 * @brief  Main function
 *****************************************************************************/
int main(void) {
    CHIP_Init();        // Chip errata
    char command[BUFFERSIZE];

    SegmentLCD_Init(false);

     // Setup SysTick Timer for 1 msec interrupts
    if (SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 1000)) {
        SegmentLCD_Write("SYSTICK");
        uart_put_data((uchar*)"Error in SysTick_Config\n\r", 25);
        while (true);
    }
    
    init_pins();
    uart_init();

    while (true) {
        SegmentLCD_Write("W CMD");
        uart_print("enter command> ");
        uart_get_command((uchar*)command);
        SegmentLCD_Write((char*)command);
        if (equals(command, "help") || equals(command, "h")) {
            uart_println("Available commands (all arguments are prompted for):");
            uart_println("help, h:      Displays this information."); 
            uart_println("debug:        Toggles debug mode on/off.");
            uart_println("inverse:      Toggles inverse input mode on/off.");
            uart_println("opto:         Toggles opto mode on/off.");
            uart_println("safety:       Toggles safety mode on/off.");
            uart_println("sweep, go, g: Probes material with all possible inputs.");
            uart_println("single, s:    Probes material with one input.");
            uart_println("pulse, p:     Applies pulse to material.");
            uart_println("cdelay:       Set circuit delay.");
            uart_println("state, modes: Show the current state of all modes.");

        } else if (equals(command, "g") || equals(command, "go") || equals(command, "sweep")) {
            SegmentLCD_Write("SWEEP");
            sweep();
            report_results();

        } else if (equals(command, "s") || equals(command, "single")) {
            SegmentLCD_Write("W DIGTS");
            uart_print("enter 4 digits> ");
            uart_get_command((uchar*)command);
            bus_t input = string_to_binary(command, 4);
            uchar result[BUS_WIDTH];
            SegmentLCD_Write("SINGLE");
            probe_material(result, input, false);
            report_result(input, result);
        
        } else if (equals(command, "state") || equals(command, "modes")) {
            report_all_modes();
        
        } else if (equals(command, "debug")) {
            debug = !debug;
            report_mode("debug", debug);
        
        } else if (equals(command, "inverse")) {
            inverse = !inverse;
            report_mode("inverse", inverse);
        
        } else if (equals(command, "opto")) {
            opto = !opto;
            report_mode("opto", opto);
        
        } else if (equals(command, "safety")) {
            safety = !safety;
            report_mode("safety", safety);
        
        } else if (equals(command, "cdelay")) {
            uart_print("Current circuit delay is ");
            uart_print_number(circuit_delay);
            uart_println(" ms.");
            uart_print("Enter new circuit delay in ms> ");
            uart_get_command((uchar*) command);
            circuit_delay = string_to_int(command);
        
        } else if (equals(command, "p") || equals(command, "pulse")) {
            SegmentLCD_Write("W FREQ");
            uart_print("enter pulse frequency in Hz> ");
            uart_get_command((uchar*)command);
            unsigned int frequency = string_to_int(command);
            if (frequency > 500) {
                uart_println("Warning: Frequency is higher than maximum");
            }
            SegmentLCD_Write("W TIME");
            uart_print("enter pulse duration in ms> ");
            uart_get_command((uchar*)command);
            unsigned int duration = string_to_int(command);
            SegmentLCD_Write("PULSE");
            apply_pulse(frequency, duration);
        
        } else if (equals(command, "delay")) {
            uart_print("enter delay time in ms> ");
            uart_get_command((uchar*)command);
            unsigned int d = string_to_int(command);
            delay(d);

        } else {
            uart_println("Unknown command. Type 'h' or 'help' for available commands.");
        }
    }
    return 0;
}

